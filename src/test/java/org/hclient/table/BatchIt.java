package org.hclient.table;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;
import org.assertj.core.api.Assertions;
import org.hclient.HbaseContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test(suiteName = "HBase batch test")
public class BatchIt {

  private static final String TEST_CF = "ts";
  private static final byte[] TEST_CF_BYTES = Bytes.toBytes(TEST_CF);
  private Table testTable;
  private TableName tableName;
  private Admin admin;

  /**
   * Object collection data provider for batch operation.
   *
   * @return
   */
  @DataProvider(name = "objCollection")
  public static Object[][] objCollection() {
    final List<String> list1 = Stream.of("v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8")
                                     .collect(toList());
    final List<String> list2 = Stream.of("v1", "v2", "v3", "v4", "v5", "v6", "v7")
                                     .collect(toList());
    final List<String> list3 = Stream.of("v1", "v2", "v3", "v4", "v5", "v6").collect(toList());
    final List<String> list4 = Stream.of("v1").collect(toList());
    final List<String> list5 = Stream.of("v1", "v2").collect(toList());
    final List<String> list6 = Collections.emptyList();
    return new Object[][]{new Object[]{list1},
                          new Object[]{list2},
                          new Object[]{list3},
                          new Object[]{list4},
                          new Object[]{list5},
                          new Object[]{list6},};
  }

  /**
   * Test class setup.
   */
  @BeforeClass
  public void setUp() throws Exception {
    final int randomTableSuffix = 5000;
    this.tableName = TableName.valueOf("testTable" + new Random().nextInt(randomTableSuffix));
    final Connection connection = HbaseContainer.connection();
    this.admin = connection.getAdmin();
    this.testTable = connection.getTable(this.tableName);

    final ColumnFamilyDescriptor cfDescriptor
        = new ColumnFamilyDescriptorBuilder.ModifyableColumnFamilyDescriptor(TEST_CF_BYTES)
                .setBloomFilterType(BloomType.ROW)
                .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                .setCompressionType(Compression.Algorithm.SNAPPY)
                .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                .setVersions(1, 1);
    final TableDescriptor descriptor
        = new TableDescriptorBuilder.ModifyableTableDescriptor(tableName)
                                    .setColumnFamily(cfDescriptor)
                                    .setCompactionEnabled(true)
                                    .setDurability(Durability.SYNC_WAL);

    if (!admin.tableExists(tableName)) {
      admin.createTable(descriptor);
    }
  }

  /**
   * Tear down test class.
   */
  @AfterClass
  public void tearDown() throws Exception {
    System.out.println("Close HBase test table");
    admin.disableTable(tableName);
    admin.deleteTable(tableName);
    testTable.close();
  }

  /**
   * Tear down test.
   */
  @AfterMethod
  public void tearDownAfterTest() throws Exception {
    final Scan scan = new Scan().readVersions(1).setFilter(new KeyOnlyFilter());
    try (ResultScanner scanner = testTable.getScanner(scan)) {
      for (Result result : scanner) {
        testTable.delete(new Delete(result.getRow()));
      }
    }
  }

  @Test(description = "Test invalid batch parameter: batch size",
        expectedExceptions = IllegalArgumentException.class)
  public void testInvalidBatchSize() throws Exception {
    final int invalidBatchSize = -20;
    Batch.newBuilder(testTable)
         .withBatchSize(invalidBatchSize)
         .on(Collections.emptyList())
         .withMapper(o -> new Put(new byte[0]))
         .execute();
  }

  @Test(description = "Test invalid batch parameter: empty object collection",
        expectedExceptions = NullPointerException.class)
  public void testInvalidObjectCollection() throws Exception {
    final int batchSize = 10;
    Batch.newBuilder(testTable)
         .withBatchSize(batchSize)
         .on((Iterator<Object>) null)
         .withMapper(o -> new Put(new byte[0]))
         .execute();
  }

  @Test(description = "Test invalid batch parameter: null mapper", expectedExceptions =
                                                                       NullPointerException.class)
  public void testInvalidMapper() throws Exception {
    final int batchSize = 10;
    Batch.newBuilder(testTable)
         .withBatchSize(batchSize)
         .on(Collections.emptyList())
         .withMapper(null)
         .execute();
  }

  @Test(description = "Test invalid batch parameter: HBase table instance",
        expectedExceptions = NullPointerException.class)
  public void testInvalidHbaseTable() throws Exception {
    final int batchSize = 10;
    Batch.newBuilder(null)
         .withBatchSize(batchSize)
         .on(Collections.emptyList())
         .withMapper(o -> new Put(new byte[0]))
         .execute();
  }

  @Test(description = "Test dynamic batch")
  public void testDynamicBatch() throws Exception {
    final byte[] dynBatchRow = Bytes.toBytes("dyn_batch_row");
    final int batchSize = 10;
    final OnDemandBatch<Integer> batch
        = OnDemandBatch.<Integer>newBuilder(testTable)
                       .withBatchSize(batchSize)
                       .withMapper(o -> new Put(dynBatchRow).addColumn(TEST_CF_BYTES,
                                                                       Bytes.toBytes("dynQf" + o),
                                                                       Bytes.toBytes(o)
                                                                      )
                                  )
                       .build();
    IntStream.range(0, batchSize).forEach(value -> {
      try {
        batch.append(value);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });
    Assertions.assertThat(testTable.get(new Get(dynBatchRow).addFamily(TEST_CF_BYTES)).listCells())
              .isNotNull()
              .hasSize(batchSize);

    batch.append(batchSize + 1);
    batch.flush();

    Assertions.assertThat(testTable.get(new Get(dynBatchRow).addFamily(TEST_CF_BYTES)).listCells())
              .isNotNull()
              .hasSize(batchSize + 1);
  }

  @Test(description = "Create valid batch instance and call batch method", dataProvider =
                                                                               "objCollection")
  public void testCreateValidBatchInstanceAndCallBatch(List<String> strCollection) throws
                                                                                   Exception {

    final byte[] valQualifier = Bytes.toBytes("val");
    final int batchSize = 3;
    Batch.<String>newBuilder(testTable).withBatchSize(batchSize)
                                       .on(strCollection)
                                       .withMapper(string -> {
                                         final byte[] key = Bytes.toBytes(string);
                                         return new Put(key).addColumn(TEST_CF_BYTES,
                                                                       valQualifier,
                                                                       key);
                                       })
                                       .execute();

    final ResultScanner scanner = testTable.getScanner(TEST_CF_BYTES, valQualifier);
    final Integer rowCount
        = StreamSupport.stream(scanner.spliterator(), false)
                       .reduce(0, (curVal, hresult) -> curVal + hresult.size(), Integer::sum);
    Assertions.assertThat(rowCount).isEqualTo(strCollection.size());
  }

  @Test(description = "Create valid batch instance and call batch method on Iterator",
        dataProvider = "objCollection")
  public void testCreateValidBatchInstanceAndCallBatchOnIterator(List<String> strCollection)
      throws Exception {

    final byte[] valQualifier = Bytes.toBytes("val");
    final int batchSize = 3;
    final Batch.Builder<String> batchBuilder
        = Batch.<String>newBuilder(testTable)
               .withBatchSize(batchSize)
               .on(strCollection.iterator())
               .withMapper(string -> {
                 final byte[] key = Bytes.toBytes(string);
                 return new Put(key).addColumn(TEST_CF_BYTES, valQualifier, key);
               });
    // batch on iterator instance perform only one time
    // other calls must have no effect(and must not fail)
    batchBuilder.execute();
    batchBuilder.execute();
    batchBuilder.execute();

    final ResultScanner scanner = testTable.getScanner(TEST_CF_BYTES, valQualifier);
    final Integer rowCount = StreamSupport.stream(scanner.spliterator(), false)
                                          .reduce(0, (curVal, hresult) -> curVal + hresult.size(),
                                                  Integer::sum);
    Assertions.assertThat(rowCount).isEqualTo(strCollection.size());
  }
}
