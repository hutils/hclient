package org.hclient;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.AsyncConnection;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.awaitility.Awaitility;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.OutputFrame;

//CHECKSTYLE:OFF
public final class HbaseContainer {

    private static final String VERSION = "2.1.3-2.8.5";
    private static final String HBASE_IMAGE_NAME = "hutils/hbase:" + VERSION;
    private static final Consumer<OutputFrame> CONTAINER_LOGGER =
        outputFrame -> System.out.print(outputFrame.getUtf8String());

    private static final GenericContainer hbase;
    private static final AsyncConnection asyncConnection;
    private static final Connection connection;

    private HbaseContainer() {}

    static {
        final Network.NetworkImpl network = Network.builder()
                                                   .driver("bridge")
                                                   .enableIpv6(false)
                                                   .build();
        hbase = new FixedHostPortGenericContainer<>(HBASE_IMAGE_NAME)
                    .withCreateContainerCmdModifier(params -> params.withHostName("localhost"))
                    .withNetwork(network)
                    .withFixedExposedPort(16000, 16000)
                    .withFixedExposedPort(16010, 16010)
                    .withFixedExposedPort(16020, 16020)
                    .withFixedExposedPort(16030, 16030)
                    .withFixedExposedPort(2181, 2181)
                    .withStartupTimeout(Duration.ofMinutes(3))
                    .withStartupAttempts(5)
                    .withCommand("-Dhbase.master.hostname=0.0.0.0",
                                 "-Dhbase.regionserver.hostname=0.0.0.0",
                                 "master start");
        hbase.start();
        hbase.followOutput(CONTAINER_LOGGER);

        final Configuration conf = gethConfig().asConfiguration();

        // await master initialization
        Awaitility.await()
                  .atMost(3, TimeUnit.MINUTES)
                  .pollInterval(1, TimeUnit.SECONDS)
                  .pollInSameThread()
                  .until(() -> {
                      try (var connection = ConnectionFactory.createConnection(conf)) {
                          connection.getAdmin().disableTable(TableName.valueOf("fake"));
                          return true;
                      }
                      catch (TableNotFoundException e) {
                          return true;
                      }
                      catch (Exception e) {
                          return false;
                      }
                  });

        try {
            asyncConnection = ConnectionFactory.createAsyncConnection(conf).get();
            connection = ConnectionFactory.createConnection(conf);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static AsyncConnection asyncConnection() {
        return asyncConnection;
    }

    public static Connection connection() {
        return connection;
    }

    private static HConfig gethConfig() {
        final String zkQuorum = zkQuorumAddr();
        final String hbaseZnode = rootZnode();

        return HConfig.newBuilder().retryCount(10)
                                   .retryBackoff(5000)
                                   .scanBatchSize(50)
                                   .scanCacheSize(50)
                                   .zkQuorum(zkQuorum)
                                   .connectionThreads(4)
                                   .metaLookupThreads(2)
                                   .metaOperationTimeout(15000)
                                   .metricsEnabled(true)
                                   .operationTimeout(15000)
                                   .perRegionMaxTasks(20)
                                   .perServerMaxTasks(40)
                                   .readRpcTimeout(15000)
                                   .writeRpcTimeout(15000)
                                   .scannerTimeout(20000)
                                   .threadPoolMaxTasks(100)
                                   .zkSessionTimeout(15000)
                                   .znode(hbaseZnode)
                                   .build();
    }

    public static String zkQuorumAddr() {
        return "localhost:2181";
    }

    public static String rootZnode() {
        return HConstants.DEFAULT_ZOOKEEPER_ZNODE_PARENT;
    }
}
//CHECKSTYLE:ON