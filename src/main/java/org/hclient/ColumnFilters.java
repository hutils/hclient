package org.hclient;

import java.util.ArrayList;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.ColumnRangeFilter;
import org.apache.hadoop.hbase.filter.FamilyFilter;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterListWithAND;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

public class ColumnFilters extends FilterListWithAND {

  public ColumnFilters() {
    super(new ArrayList<>());
  }

  public ColumnFilters withPrefix(String prefix) {
    return withPrefix(Bytes.toBytes(prefix));
  }

  public ColumnFilters withPrefix(int prefix) {
    return withPrefix(Bytes.toBytes(prefix));
  }

  public ColumnFilters withPrefix(long prefix) {
    return withPrefix(Bytes.toBytes(prefix));
  }

  public ColumnFilters withPrefix(short prefix) {
    return withPrefix(Bytes.toBytes(prefix));
  }

  public ColumnFilters withPrefix(byte prefix) {
    return withPrefix(Bytes.toBytes(prefix));
  }

  public ColumnFilters withPrefix(byte[] prefix) {
    return addNewFilter(new ColumnPrefixFilter(prefix));
  }

  /**
   * Add range column filter.
   * @param minColumn Minimum column value, e.g. range start
   * @param minColumnInclusive Is range start value inclusive
   * @param maxColumn Maximum column value, e.g. range end
   * @param maxColumnInclusive Is range end value inclusive
   */
  public ColumnFilters inRange(byte[] minColumn,
                               boolean minColumnInclusive,
                               byte[] maxColumn,
                               boolean maxColumnInclusive) {

    final var rangeFilter = new ColumnRangeFilter(minColumn,
                                                  minColumnInclusive,
                                                  maxColumn,
                                                  maxColumnInclusive);
    return addNewFilter(rangeFilter);
  }

  /**
   * Add range column filter.
   * @param minColumn Minimum column value, e.g. range start
   * @param minColumnInclusive Is range start value inclusive
   * @param maxColumn Maximum column value, e.g. range end
   * @param maxColumnInclusive Is range end value inclusive
   */
  public ColumnFilters inRange(String minColumn,
                               boolean minColumnInclusive,
                               String maxColumn,
                               boolean maxColumnInclusive) {

    return inRange(Bytes.toBytes(minColumn),
                   minColumnInclusive,
                   Bytes.toBytes(maxColumn),
                   maxColumnInclusive);
  }

  /**
   * Add range column filter.
   * @param minColumn Minimum column value, e.g. range start
   * @param minColumnInclusive Is range start value inclusive
   * @param maxColumn Maximum column value, e.g. range end
   * @param maxColumnInclusive Is range end value inclusive
   */
  public ColumnFilters inRange(int minColumn,
                               boolean minColumnInclusive,
                               int maxColumn,
                               boolean maxColumnInclusive) {

    return inRange(Bytes.toBytes(minColumn),
                   minColumnInclusive,
                   Bytes.toBytes(maxColumn),
                   maxColumnInclusive);
  }

  /**
   * Add range column filter.
   * @param minColumn Minimum column value, e.g. range start
   * @param minColumnInclusive Is range start value inclusive
   * @param maxColumn Maximum column value, e.g. range end
   * @param maxColumnInclusive Is range end value inclusive
   */
  public ColumnFilters inRange(long minColumn,
                               boolean minColumnInclusive,
                               long maxColumn,
                               boolean maxColumnInclusive) {

    return inRange(Bytes.toBytes(minColumn),
                   minColumnInclusive,
                   Bytes.toBytes(maxColumn),
                   maxColumnInclusive);
  }

  /**
   * Add range column filter.
   * @param minColumn Minimum column value, e.g. range start
   * @param minColumnInclusive Is range start value inclusive
   * @param maxColumn Maximum column value, e.g. range end
   * @param maxColumnInclusive Is range end value inclusive
   */
  public ColumnFilters inRange(short minColumn,
                               boolean minColumnInclusive,
                               short maxColumn,
                               boolean maxColumnInclusive) {

    return inRange(Bytes.toBytes(minColumn),
                   minColumnInclusive,
                   Bytes.toBytes(maxColumn),
                   maxColumnInclusive);
  }

  /**
   * Add range column filter.
   * @param minColumn Minimum column value, e.g. range start
   * @param minColumnInclusive Is range start value inclusive
   * @param maxColumn Maximum column value, e.g. range end
   * @param maxColumnInclusive Is range end value inclusive
   */
  public ColumnFilters inRange(byte minColumn,
                               boolean minColumnInclusive,
                               byte maxColumn,
                               boolean maxColumnInclusive) {

    return inRange(Bytes.toBytes(minColumn),
                   minColumnInclusive,
                   Bytes.toBytes(maxColumn),
                   maxColumnInclusive);
  }

  /**
   * Add filter is column belong to family.
   * @param colFamily Column family to match.
   */
  public ColumnFilters hasFamily(byte[] colFamily) {
    return addNewFilter(new FamilyFilter(CompareOperator.EQUAL, new BinaryComparator(colFamily)));
  }

  /**
   * Add filter is column belong to family.
   * @param colFamily Column family to match.
   */
  public ColumnFilters hasFamily(String colFamily) {
    return hasFamily(Bytes.toBytes(colFamily));
  }

  /**
   * Add column match filter, e.g. is cell belong to given column qualifier.
   * @param qualifier Column qualifier to match.
   */
  public ColumnFilters hasQualifier(byte[] qualifier) {
    return addNewFilter(new QualifierFilter(CompareOperator.EQUAL,
                                            new BinaryComparator(qualifier)));
  }

  /**
   * Add column match filter, e.g. is cell belong to given column qualifier.
   * @param qualifier Column qualifier to match.
   */
  public ColumnFilters hasQualifier(String qualifier) {
    return hasQualifier(Bytes.toBytes(qualifier));
  }

  /**
   * Add column match filter, e.g. is cell belong to given column qualifier.
   * @param qualifier Column qualifier to match.
   */
  public ColumnFilters hasQualifier(long qualifier) {
    return hasQualifier(Bytes.toBytes(qualifier));
  }

  /**
   * Add column match filter, e.g. is cell belong to given column qualifier.
   * @param qualifier Column qualifier to match.
   */
  public ColumnFilters hasQualifier(int qualifier) {
    return hasQualifier(Bytes.toBytes(qualifier));
  }

  /**
   * Add column match filter, e.g. is cell belong to given column qualifier.
   * @param qualifier Column qualifier to match.
   */
  public ColumnFilters hasQualifier(short qualifier) {
    return hasQualifier(Bytes.toBytes(qualifier));
  }

  /**
   * Add column match filter, e.g. is cell belong to given column qualifier.
   * @param qualifier Column qualifier to match.
   */
  public ColumnFilters hasQualifier(byte qualifier) {
    return hasQualifier(Bytes.toBytes(qualifier));
  }

  /**
   * Add filter to check cell value.
   * @param value Expected cell value.
   */
  public ColumnFilters hasValue(byte[] value) {
    return addNewFilter(new ValueFilter(CompareOperator.EQUAL, new BinaryComparator(value)));
  }

  /**
   * Add filter to check cell value.
   * @param value Expected cell value.
   */
  public ColumnFilters hasValue(int value) {
    return hasValue(Bytes.toBytes(value));
  }

  /**
   * Add filter to check cell value.
   * @param value Expected cell value.
   */
  public ColumnFilters hasValue(long value) {
    return hasValue(Bytes.toBytes(value));
  }

  /**
   * Add filter to check cell value.
   * @param value Expected cell value.
   */
  public ColumnFilters hasValue(short value) {
    return hasValue(Bytes.toBytes(value));
  }

  /**
   * Add filter to check cell value.
   * @param value Expected cell value.
   */
  public ColumnFilters hasValue(byte value) {
    return hasValue(Bytes.toBytes(value));
  }

  private ColumnFilters addNewFilter(Filter filter) {
    super.getFilters().add(filter);
    return this;
  }
}
