package org.hclient;

import java.io.Serializable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HConstants;

/**
 * HBase configuration class which represent main config options and reasonable defaults for it.
 */
public final class HConfig implements Serializable {

  private static final long serialVersionUID = -6716196443660772611L;
  private final String zkQuorum;
  private final int scanBatchSize;
  private final int scanCacheSize;
  private final int retryCount;
  private final int retryBackoff;
  private final int hconnectionThreads;
  private final int readRpcTimeout;
  private final int writeRpcTimeout;
  private final String znode;
  private final int zkSessionTimeout;
  private final int perRegionMaxTasks;
  private final int perServerMaxTasks;
  private final int scannerTimeout;
  private final int metaOperationTimeout;
  private final int operationTimeout;
  private final boolean metricsEnabled;
  private final int metaLookupThreads;
  private final int threadPoolMaxTasks;
  private transient Configuration configuration;

  private HConfig(final Builder builder) {
    zkQuorum = builder.zkQuorum;
    scanBatchSize = builder.scanBatchSize;
    scanCacheSize = builder.scanCacheSize;
    retryCount = builder.retryCount;
    retryBackoff = builder.retryBackoff;
    hconnectionThreads = builder.hconnectionThreads;
    readRpcTimeout = builder.readRpcTimeout;
    writeRpcTimeout = builder.writeRpcTimeout;
    znode = builder.znode;
    zkSessionTimeout = builder.zkSessionTimeout;
    perRegionMaxTasks = builder.perRegionMaxTasks;
    perServerMaxTasks = builder.perServerMaxTasks;
    scannerTimeout = builder.scannerTimeout;
    metaOperationTimeout = builder.metaOperationTimeout;
    operationTimeout = builder.operationTimeout;
    metricsEnabled = builder.metricsEnabled;
    metaLookupThreads = builder.metaLookupThreads;
    threadPoolMaxTasks = builder.threadPoolMaxTasks;
  }

  public static Builder newBuilder() {
    return new Builder();
  }

  public String zkQuorum() {
    return zkQuorum;
  }

  public int scanBatchSize() {
    return scanBatchSize;
  }

  public int scanCacheSize() {
    return scanCacheSize;
  }

  public int retryCount() {
    return retryCount;
  }

  public int retryBackoff() {
    return retryBackoff;
  }

  public int connectionThreads() {
    return hconnectionThreads;
  }

  public int readRpcTimeout() {
    return readRpcTimeout;
  }

  public int writeRpcTimeout() {
    return writeRpcTimeout;
  }

  public String znode() {
    return znode;
  }

  public int zkSessionTimeout() {
    return zkSessionTimeout;
  }

  public int perRegionMaxTasks() {
    return perRegionMaxTasks;
  }

  public int perServerMaxTasks() {
    return perServerMaxTasks;
  }

  public int scannerTimeout() {
    return scannerTimeout;
  }

  public int metaOperationTimeout() {
    return metaOperationTimeout;
  }

  public int operationTimeout() {
    return operationTimeout;
  }

  public boolean isMetricsEnabled() {
    return metricsEnabled;
  }

  public int metaLookupThreads() {
    return metaLookupThreads;
  }

  public int threadPoolMaxTasks() {
    return threadPoolMaxTasks;
  }

  /**
   * Return current config as {@link HBaseConfiguration} with properties which
   * can be filled in it.
   *
   * @return
   */
  public Configuration asConfiguration() {
    if (configuration == null) {
      configuration = HBaseConfiguration.create();
    }

    configuration.set(HConstants.ZOOKEEPER_QUORUM, zkQuorum);
    configuration.set(HConstants.ZOOKEEPER_ZNODE_PARENT, znode);
    configuration.setInt(HConstants.ZK_SESSION_TIMEOUT, zkSessionTimeout);

    configuration.setInt(HConstants.HBASE_CLIENT_SCANNER_CACHING, scanCacheSize);
    configuration.setInt(HConstants.HBASE_CLIENT_SCANNER_TIMEOUT_PERIOD, scannerTimeout);

    configuration.setInt(HConstants.HBASE_RPC_READ_TIMEOUT_KEY, readRpcTimeout);
    configuration.setInt(HConstants.HBASE_RPC_WRITE_TIMEOUT_KEY, writeRpcTimeout);
    configuration.setInt(HConstants.HBASE_CLIENT_RETRIES_NUMBER, retryCount);
    configuration.setInt(HConstants.HBASE_CLIENT_PAUSE, retryBackoff);
    configuration.setInt(HConstants.HBASE_CLIENT_META_OPERATION_TIMEOUT, metaOperationTimeout);
    configuration.setInt(HConstants.HBASE_CLIENT_OPERATION_TIMEOUT, operationTimeout);
    configuration.setBoolean("hbase.client.metrics.enable", metricsEnabled);

    configuration.setInt("hbase.hconnection.threads.max", hconnectionThreads);
    configuration.setInt("hbase.hconnection.threads.core", hconnectionThreads);
    configuration.setInt("hbase.hconnection.meta.lookup.threads.max", metaLookupThreads);
    configuration.setInt("hbase.hconnection.meta.lookup.threads.core", metaLookupThreads);

    configuration.setInt(HConstants.HBASE_CLIENT_MAX_PERREGION_TASKS, perRegionMaxTasks);
    configuration.setInt(HConstants.HBASE_CLIENT_MAX_PERSERVER_TASKS, perServerMaxTasks);
    configuration.setInt(HConstants.HBASE_CLIENT_MAX_TOTAL_TASKS, threadPoolMaxTasks);

    return configuration;
  }

  /**
   * Create copy of current {@link HConfig} as builder instance which can be modified.
   *
   * @return
   */
  public Builder copy() {
    return newBuilder().zkQuorum(zkQuorum)
                       .scanBatchSize(scanBatchSize)
                       .scanCacheSize(scanCacheSize)
                       .retryCount(retryCount)
                       .retryBackoff(retryBackoff);
  }

  public static final class Builder {

    private String zkQuorum = "localhost:2181";
    private int scanBatchSize = HConstants.DEFAULT_HBASE_CLIENT_SCANNER_CACHING;
    private int scanCacheSize = HConstants.DEFAULT_HBASE_CLIENT_SCANNER_CACHING;
    private int retryCount = 1;
    private int retryBackoff = 1000;
    private int hconnectionThreads = Runtime.getRuntime().availableProcessors() * 8;
    private int readRpcTimeout = HConstants.DEFAULT_HBASE_RPC_TIMEOUT;
    private int writeRpcTimeout = HConstants.DEFAULT_HBASE_RPC_TIMEOUT;
    private String znode = HConstants.DEFAULT_ZOOKEEPER_ZNODE_PARENT;
    private int zkSessionTimeout = HConstants.DEFAULT_ZK_SESSION_TIMEOUT;
    private int perRegionMaxTasks = HConstants.DEFAULT_HBASE_CLIENT_MAX_PERREGION_TASKS;
    private int perServerMaxTasks = HConstants.DEFAULT_HBASE_CLIENT_MAX_PERSERVER_TASKS;
    private int scannerTimeout = HConstants.DEFAULT_HBASE_CLIENT_SCANNER_TIMEOUT_PERIOD;
    private int metaOperationTimeout = HConstants.DEFAULT_HBASE_CLIENT_OPERATION_TIMEOUT;
    private int operationTimeout = HConstants.DEFAULT_HBASE_CLIENT_OPERATION_TIMEOUT;
    private boolean metricsEnabled;
    private int metaLookupThreads = Runtime.getRuntime().availableProcessors() * 2;
    private int threadPoolMaxTasks = HConstants.DEFAULT_HBASE_CLIENT_MAX_TOTAL_TASKS;

    private Builder() {}

    public Builder zkQuorum(final String zkQuorum) {
      this.zkQuorum = zkQuorum;
      return this;
    }

    public Builder scanBatchSize(final int scanBatchSize) {
      this.scanBatchSize = scanBatchSize;
      return this;
    }

    public Builder scanCacheSize(final int scanCacheSize) {
      this.scanCacheSize = scanCacheSize;
      return this;
    }

    public Builder retryCount(final int retryCount) {
      this.retryCount = retryCount;
      return this;
    }

    public Builder retryBackoff(final int retryBackoff) {
      this.retryBackoff = retryBackoff;
      return this;
    }

    public Builder connectionThreads(final int threads) {
      this.hconnectionThreads = threads;
      return this;
    }

    public Builder readRpcTimeout(final int value) {
      this.readRpcTimeout = value;
      return this;
    }

    public Builder writeRpcTimeout(final int value) {
      this.writeRpcTimeout = value;
      return this;
    }

    public Builder znode(final String value) {
      this.znode = value;
      return this;
    }

    public Builder zkSessionTimeout(final int value) {
      this.zkSessionTimeout = value;
      return this;
    }

    public Builder perRegionMaxTasks(final int value) {
      this.perRegionMaxTasks = value;
      return this;
    }

    public Builder perServerMaxTasks(final int value) {
      this.perServerMaxTasks = value;
      return this;
    }

    public Builder scannerTimeout(final int value) {
      this.scannerTimeout = value;
      return this;
    }

    public Builder metaOperationTimeout(final int value) {
      this.metaOperationTimeout = value;
      return this;
    }

    public Builder operationTimeout(final int value) {
      this.operationTimeout = value;
      return this;
    }

    public Builder metricsEnabled(final boolean value) {
      this.metricsEnabled = value;
      return this;
    }

    public Builder metaLookupThreads(final int value) {
      this.metaLookupThreads = value;
      return this;
    }

    public Builder threadPoolMaxTasks(final int value) {
      this.threadPoolMaxTasks = value;
      return this;
    }

    public HConfig build() {
      return new HConfig(this);
    }
  }
}
