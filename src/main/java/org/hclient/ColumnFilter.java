package org.hclient;

/**
 * Contains information to filter result from hbase table.
 */
public final class ColumnFilter {
  private byte[] columnFamily;
  private byte[] qualifier;

  public ColumnFilter(byte[] columnFamily) {
    this(columnFamily, new byte[0]);
  }

  public ColumnFilter(byte[] columnFamily, byte[] qualifier) {
    this.columnFamily = columnFamily;
    this.qualifier = qualifier;
  }

  /**
   * Column family to filter by.
   */
  public byte[] getColumnFamily() {
    return columnFamily;
  }

  /**
   * Column family to filter by.
   */
  public void setColumnFamily(byte[] columnFamily) {
    this.columnFamily = columnFamily;
  }

  /**
   * Qualifier to filter by.
   */
  public byte[] getQualifier() {
    return qualifier;
  }

  /**
   * Qualifier to filter by.
   */
  public void setQualifier(byte[] qualifier) {
    this.qualifier = qualifier;
  }
}
