package org.hclient;

import java.io.Serializable;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;

/**
 * HBase cell class which may be serialized.
 */
public class SerializableCell implements Cell, Serializable {

  private static final long serialVersionUID = 1716447423299472697L;

  private final byte[] rowKey;
  private final byte[] family;
  private final byte[] qualifier;
  private final byte[] value;

  private final Type type;
  private final long timestamp;
  private final byte[] tags;
  private final long sequenceId;

  /**
   * Copy data from {@code Cell} instance.
   *
   * @param cell Source cell.
   */
  public SerializableCell(Cell cell) {
    rowKey = CellUtil.cloneRow(cell);
    family = CellUtil.cloneFamily(cell);
    qualifier = CellUtil.cloneQualifier(cell);
    value = CellUtil.cloneValue(cell);
    tags = CellUtil.cloneTags(cell);
    sequenceId = cell.getSequenceId();
    timestamp = cell.getTimestamp();
    type = cell.getType();
  }

  /**
   * Is this cell has matched family and qualifier.
   *
   * @param family Family to compare.
   * @param qualifier Coluemn qualifier to compare.
   * @return
   */
  public boolean matches(byte[] family, byte[] qualifier) {
    return CellUtil.matchingColumn(this, family, qualifier);
  }

  @Override
  public byte[] getRowArray() {
    return rowKey;
  }

  @Override
  public int getRowOffset() {
    return 0;
  }

  @Override
  public short getRowLength() {
    return (short) rowKey.length;
  }

  @Override
  public byte[] getFamilyArray() {
    return family;
  }

  @Override
  public int getFamilyOffset() {
    return 0;
  }

  @Override
  public byte getFamilyLength() {
    return (byte) family.length;
  }

  @Override
  public byte[] getQualifierArray() {
    return qualifier;
  }

  @Override
  public int getQualifierOffset() {
    return 0;
  }

  @Override
  public int getQualifierLength() {
    return qualifier.length;
  }

  @Override
  public long getTimestamp() {
    return timestamp;
  }

  @Override
  public byte getTypeByte() {
    return type.getCode();
  }

  @Override
  public long getSequenceId() {
    return sequenceId;
  }

  @Override
  public byte[] getValueArray() {
    return value;
  }

  @Override
  public int getValueOffset() {
    return 0;
  }

  @Override
  public int getValueLength() {
    return value.length;
  }

  @Override
  public byte[] getTagsArray() {
    return tags;
  }

  @Override
  public int getTagsOffset() {
    return 0;
  }

  @Override
  public int getTagsLength() {
    return tags.length;
  }
}
