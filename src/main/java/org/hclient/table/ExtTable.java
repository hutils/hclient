package org.hclient.table;

import java.io.IOException;
import java.util.List;
import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.Table;

public interface ExtTable extends Table {

  <T> Batch.Builder<T> batch();

  BatchResult batch(List<? extends Row> actions) throws IOException, InterruptedException;

  <T> OnDemandBatch.Builder<T> onDemandBatch();

  ExtScan scan();
}
