package org.hclient.table;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.Table;

/**
 * Class perform batch of HBase row operations(puts/increment/delete) for object collection by
 * mapping it into collection of {@link Row}.
 * <p>
 * Each batch divided into parts, e.g. collection is broken into splits of with predefined size.
 * For each split we execute {@code Table.batch()} defined in HBase client API.
 * </p>
 */
final class Batch<T> {

  private Iterable<? extends T> objectCollection;
  private int batchSize;
  private Function<T, Row> objectMapper;
  private Table table;

  private Batch(final Builder builder) {
    objectCollection = builder.objectCollection;
    batchSize = builder.batchSize;
    objectMapper = builder.objectMapper;
    table = builder.table;
  }

  /**
   * Create new {@link Batch.Builder} instance.
   *
   * @return
   */
  static <T> Builder<T> newBuilder(Table table) {
    return new Builder<>(table);
  }

  /**
   * Method perform a HBase batch operation.
   */
  public void call() throws Exception {
    final var rowList = new ArrayList<Row>(batchSize);
    final var results = new BatchResult(batchSize);
    for (T object : objectCollection) {
      final Row row = objectMapper.apply(object);
      rowList.add(row);
      //reach batch limit size, flush index data to HBase
      if (rowList.size() >= batchSize) {
        table.batch(rowList, results.asArray());
        results.throwIfFailed();
        rowList.clear();
      }
    }
    //save remaining index data
    if (!rowList.isEmpty()) {
      final var errors = new BatchResult(rowList.size());
      table.batch(rowList, errors.asArray());
      errors.throwIfFailed();
    }
  }

  /**
   * {@code Batch} builder static inner class.
   */
  public static final class Builder<T> {

    private Table table;
    private Iterable<? extends T> objectCollection = Collections.emptyList();
    private int batchSize = 50;
    private Function<T, Row> objectMapper;

    private Builder(Table table) {
      this.table = table;
    }

    /**
     * Set object collection which should be splitted and batched.
     *
     * @param val Collection instance
     */
    public Builder<T> on(final Iterable<? extends T> val) {
      requireNonNull(val);
      objectCollection = val;
      return this;
    }

    /**
     * Set object collection which should be splitted and batched.
     *
     * @param val Collection iterator
     */
    public Builder<T> on(final Iterator<T> val) {
      requireNonNull(val);
      objectCollection = () -> val;
      return this;
    }

    /**
     * Batch size.
     */
    public Builder<T> withBatchSize(final int val) {
      if (val <= 0) {
        throw new IllegalArgumentException("Batch size cannot be less than 0");
      }
      batchSize = val;
      return this;
    }

    /**
     * Set object collection element mapper. This function should map item from collection
     * into instance of {@link Row}.
     */
    public Builder<T> withMapper(Function<T, Row> objectMapper) {
      requireNonNull(objectMapper);
      this.objectMapper = objectMapper;
      return this;
    }

    /**
     * Build batch instance.
     *
     * @return
     */
    Batch<T> build() {
      requireNonNull(table, "HBase Table not set");
      requireNonNull(objectMapper, "Object to HBase row instance mapper not set");
      return new Batch<>(this);
    }

    /**
     * Execute batch on passed collection.
     */
    public void execute() throws Exception {
      requireNonNull(table, "HBase Table not set");
      requireNonNull(objectMapper, "Object to HBase row instance mapper not set");

      new Batch<>(this).call();
    }
  }
}
