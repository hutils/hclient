package org.hclient.table;

import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

public class ExtScan extends Scan {

  public Scan withStartRow(String startRow) {
    return super.withStartRow(Bytes.toBytes(startRow));
  }

  public Scan withStartRow(String startRow, boolean inclusive) {
    return super.withStartRow(Bytes.toBytes(startRow), inclusive);
  }

  public Scan withStartRow(int startRow) {
    return super.withStartRow(Bytes.toBytes(startRow));
  }

  public Scan withStartRow(int startRow, boolean inclusive) {
    return super.withStartRow(Bytes.toBytes(startRow), inclusive);
  }

  public Scan withStartRow(long startRow) {
    return super.withStartRow(Bytes.toBytes(startRow));
  }

  public Scan withStartRow(long startRow, boolean inclusive) {
    return super.withStartRow(Bytes.toBytes(startRow), inclusive);
  }

  public Scan withStartRow(short startRow) {
    return super.withStartRow(Bytes.toBytes(startRow));
  }

  public Scan withStartRow(short startRow, boolean inclusive) {
    return super.withStartRow(Bytes.toBytes(startRow), inclusive);
  }

  public Scan withStartRow(byte startRow) {
    return super.withStartRow(Bytes.toBytes(startRow));
  }

  public Scan withStartRow(byte startRow, boolean inclusive) {
    return super.withStartRow(Bytes.toBytes(startRow), inclusive);
  }
}
