package org.hclient.table;

import java.io.IOException;
import java.util.List;
import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.Table;

final class ExtTableImpl extends DelegatingTable implements ExtTable {

  public ExtTableImpl(Table table) {
    super(table);
  }

  @Override
  public <T> Batch.Builder<T> batch() {
    return Batch.newBuilder(innerTable);
  }

  @Override
  public BatchResult batch(List<? extends Row> actions) throws IOException {
    final var batchResult = new BatchResult(actions.size());
    try {
      super.batch(actions, batchResult.asArray());
    } catch (InterruptedException e) {
      throw new IOException("IO interrupted", e);
    }
    return batchResult;
  }

  @Override
  public <T> OnDemandBatch.Builder<T> onDemandBatch() {
    return OnDemandBatch.newBuilder(innerTable);
  }

  @Override
  public ExtScan scan() {
    return new ExtScan();
  }
}
