package org.hclient.table;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Append;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Increment;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.RowMutations;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.coprocessor.Batch;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.ipc.CoprocessorRpcChannel;
import org.apache.hadoop.hbase.shaded.com.google.protobuf.Descriptors;
import org.apache.hadoop.hbase.shaded.com.google.protobuf.Message;
import org.apache.hadoop.hbase.shaded.com.google.protobuf.Service;
import org.apache.hadoop.hbase.shaded.com.google.protobuf.ServiceException;

public abstract class DelegatingTable implements Table {

  protected final Table innerTable;

  public DelegatingTable(Table innerTable) {
    this.innerTable = innerTable;
  }

  @Override
  public TableName getName() {
    return innerTable.getName();
  }

  @Override
  public Configuration getConfiguration() {
    return innerTable.getConfiguration();
  }

  @Deprecated
  @Override
  public HTableDescriptor getTableDescriptor() throws IOException {
    return innerTable.getTableDescriptor();
  }

  @Override
  public TableDescriptor getDescriptor() throws IOException {
    return innerTable.getDescriptor();
  }

  @Override
  public boolean exists(Get get) throws IOException {
    return innerTable.exists(get);
  }

  @Override
  public boolean[] exists(List<Get> gets) throws IOException {
    return innerTable.exists(gets);
  }

  @Override
  @Deprecated
  public boolean[] existsAll(List<Get> gets) throws IOException {
    return innerTable.existsAll(gets);
  }

  @Override
  public void batch(List<? extends Row> actions, Object[] results) throws
                                                                   IOException,
                                                                   InterruptedException {
    innerTable.batch(actions, results);
  }

  @Override
  public <R> void batchCallback(List<? extends Row> actions,
                                Object[] results,
                                Batch.Callback<R> callback) throws
                                                            IOException,
                                                            InterruptedException {
    innerTable.batchCallback(actions, results, callback);
  }

  @Override
  public Result get(Get get) throws IOException {
    return innerTable.get(get);
  }

  @Override
  public Result[] get(List<Get> gets) throws IOException {
    return innerTable.get(gets);
  }

  @Override
  public ResultScanner getScanner(Scan scan) throws IOException {
    return innerTable.getScanner(scan);
  }

  @Override
  public ResultScanner getScanner(byte[] family) throws IOException {
    return innerTable.getScanner(family);
  }

  @Override
  public ResultScanner getScanner(byte[] family, byte[] qualifier) throws IOException {
    return innerTable.getScanner(family, qualifier);
  }

  @Override
  public void put(Put put) throws IOException {
    innerTable.put(put);
  }

  @Override
  public void put(List<Put> puts) throws IOException {
    innerTable.put(puts);
  }

  @Override
  @Deprecated
  public boolean checkAndPut(byte[] row,
                             byte[] family,
                             byte[] qualifier,
                             byte[] value,
                             Put put) throws IOException {
    return innerTable.checkAndPut(row, family, qualifier, value, put);
  }

  @Override
  @Deprecated
  public boolean checkAndPut(byte[] row,
                             byte[] family,
                             byte[] qualifier,
                             CompareFilter.CompareOp compareOp,
                             byte[] value,
                             Put put) throws IOException {
    return innerTable.checkAndPut(row, family, qualifier, compareOp, value, put);
  }

  @Override
  @Deprecated
  public boolean checkAndPut(byte[] row,
                             byte[] family,
                             byte[] qualifier,
                             CompareOperator op,
                             byte[] value,
                             Put put) throws IOException {
    return innerTable.checkAndPut(row, family, qualifier, op, value, put);
  }

  @Override
  public void delete(Delete delete) throws IOException {
    innerTable.delete(delete);
  }

  @Override
  public void delete(List<Delete> deletes) throws IOException {
    innerTable.delete(deletes);
  }

  @Override
  @Deprecated
  public boolean checkAndDelete(byte[] row,
                                byte[] family,
                                byte[] qualifier,
                                byte[] value,
                                Delete delete) throws IOException {
    return innerTable.checkAndDelete(row, family, qualifier, value, delete);
  }

  @Override
  @Deprecated
  public boolean checkAndDelete(byte[] row,
                                byte[] family,
                                byte[] qualifier,
                                CompareFilter.CompareOp compareOp,
                                byte[] value,
                                Delete delete) throws IOException {

    return innerTable.checkAndDelete(row, family, qualifier, compareOp, value, delete);
  }

  @Override
  @Deprecated
  public boolean checkAndDelete(byte[] row,
                                byte[] family,
                                byte[] qualifier,
                                CompareOperator op,
                                byte[] value,
                                Delete delete) throws IOException {

    return innerTable.checkAndDelete(row, family, qualifier, op, value, delete);
  }

  @Override
  public void mutateRow(RowMutations rm) throws IOException {
    innerTable.mutateRow(rm);
  }

  @Override
  public Result append(Append append) throws IOException {
    return innerTable.append(append);
  }

  @Override
  public Result increment(Increment increment) throws IOException {
    return innerTable.increment(increment);
  }

  @Override
  public long incrementColumnValue(byte[] row, byte[] family, byte[] qualifier, long amount)
      throws IOException {

    return innerTable.incrementColumnValue(row, family, qualifier, amount);
  }

  @Override
  public long incrementColumnValue(byte[] row,
                                   byte[] family,
                                   byte[] qualifier,
                                   long amount,
                                   Durability durability) throws IOException {
    return innerTable.incrementColumnValue(row, family, qualifier, amount, durability);
  }

  @Override
  public void close() throws IOException {
    innerTable.close();
  }

  @Override
  public CoprocessorRpcChannel coprocessorService(byte[] row) {
    return innerTable.coprocessorService(row);
  }

  @Override
  public <T extends Service, R> Map<byte[], R> coprocessorService(
      Class<T> service,
      byte[] startKey,
      byte[] endKey,
      Batch.Call<T, R> callable) throws ServiceException, Throwable {

    return innerTable.coprocessorService(service, startKey, endKey, callable);
  }

  @Override
  public <T extends Service, R> void coprocessorService(
      Class<T> service,
      byte[] startKey,
      byte[] endKey,
      Batch.Call<T, R> callable,
      Batch.Callback<R> callback) throws ServiceException, Throwable {

    innerTable.coprocessorService(service, startKey, endKey, callable, callback);
  }

  @Override
  public <R extends Message> Map<byte[], R> batchCoprocessorService(
      Descriptors.MethodDescriptor methodDescriptor,
      Message request,
      byte[] startKey,
      byte[] endKey,
      R responsePrototype) throws ServiceException, Throwable {

    return innerTable.batchCoprocessorService(methodDescriptor,
                                              request,
                                              startKey,
                                              endKey,
                                              responsePrototype);
  }

  @Override
  public <R extends Message> void batchCoprocessorService(
      Descriptors.MethodDescriptor methodDescriptor,
      Message request,
      byte[] startKey,
      byte[] endKey,
      R responsePrototype,
      Batch.Callback<R> callback) throws ServiceException, Throwable {

    innerTable.batchCoprocessorService(methodDescriptor,
                                       request,
                                       startKey,
                                       endKey,
                                       responsePrototype,
                                       callback);
  }

  @Override
  public CheckAndMutateBuilder checkAndMutate(byte[] row, byte[] family) {
    return innerTable.checkAndMutate(row, family);
  }

  @Override
  @Deprecated
  public boolean checkAndMutate(byte[] row,
                                byte[] family,
                                byte[] qualifier,
                                CompareFilter.CompareOp compareOp,
                                byte[] value,
                                RowMutations mutation) throws IOException {
    return innerTable.checkAndMutate(row, family, qualifier, compareOp, value, mutation);
  }

  @Override
  @Deprecated
  public boolean checkAndMutate(byte[] row,
                                byte[] family,
                                byte[] qualifier,
                                CompareOperator op,
                                byte[] value,
                                RowMutations mutation) throws IOException {
    return innerTable.checkAndMutate(row, family, qualifier, op, value, mutation);
  }

  @Override
  public long getRpcTimeout(TimeUnit unit) {
    return innerTable.getRpcTimeout(unit);
  }

  @Override
  @Deprecated
  public int getRpcTimeout() {
    return innerTable.getRpcTimeout();
  }

  @Override
  @Deprecated
  public void setRpcTimeout(int rpcTimeout) {
    innerTable.setRpcTimeout(rpcTimeout);
  }

  @Override
  public long getReadRpcTimeout(TimeUnit unit) {
    return innerTable.getReadRpcTimeout(unit);
  }

  @Override
  @Deprecated
  public int getReadRpcTimeout() {
    return innerTable.getReadRpcTimeout();
  }

  @Override
  @Deprecated
  public void setReadRpcTimeout(int readRpcTimeout) {
    innerTable.setReadRpcTimeout(readRpcTimeout);
  }

  @Override
  public long getWriteRpcTimeout(TimeUnit unit) {
    return innerTable.getWriteRpcTimeout(unit);
  }

  @Override
  @Deprecated
  public int getWriteRpcTimeout() {
    return innerTable.getWriteRpcTimeout();
  }

  @Override
  @Deprecated
  public void setWriteRpcTimeout(int writeRpcTimeout) {
    innerTable.setWriteRpcTimeout(writeRpcTimeout);
  }

  @Override
  public long getOperationTimeout(TimeUnit unit) {
    return innerTable.getOperationTimeout(unit);
  }

  @Override
  @Deprecated
  public int getOperationTimeout() {
    return innerTable.getOperationTimeout();
  }

  @Override
  @Deprecated
  public void setOperationTimeout(int operationTimeout) {
    innerTable.setOperationTimeout(operationTimeout);
  }
}
