package org.hclient.table;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.Table;

/**
 * On demand batch class execute HBase batch operation when internal
 * items buffer reach predefined batch size.
 * <p>
 * Main difference of this class with {@link Batch} is internal buffer
 * which aggregate items until batch executed.
 * This is very useful when you don't know how many items will be batched.
 * </p>
 * Usage pattern:
 * <pre>
 * {@code
 *   OnDemandBatch<Object> onDemandBatch = ...;
 *
 *   while (hasMoreElements()) {
 *     Object next = readNextElement();
 *     // add next element. This call can execute HBase batch operation
 *     // if internal buffer reach predefined size.
 *     onDemandBatch.append(next);
 *   }
 *   // if internal buffer contains items, we will flush them. Otherwise, no-op.
 *   onDemandBatch.flush();
 * }
 * </pre>
 *
 * @param <T> Items in batch
 */
public final class OnDemandBatch<T> {

  private final Batch<T> batch;
  private List<T> objectCollection;
  private int batchSize;
  private Function<T, Row> objectMapper;
  private Table table;

  private OnDemandBatch(final Builder<T> builder) {
    objectCollection = new ArrayList<>(builder.batchSize);
    batchSize = builder.batchSize;
    objectMapper = builder.objectMapper;
    table = builder.table;
    batch = Batch.<T>newBuilder(table).withBatchSize(batchSize)
                                      .withMapper(objectMapper)
                                      .on(objectCollection)
                                      .build();
  }

  static <T> Builder<T> newBuilder(Table table) {
    return new Builder<>(table);
  }

  /**
   * Append element to batch collection and if max size reached then execute batch operation.
   *
   * @param element Batch element instance to add.
   * @throws Exception Error while batch flushing.
   */
  public void append(T element) throws Exception {
    objectCollection.add(element);
    if (objectCollection.size() >= batchSize) {
      flush();
    }
  }

  /**
   * Method called when last element already added to batch collection and batch operation have
   * to be forced on remaining elements.
   */
  public void flush() throws Exception {
    if (objectCollection.isEmpty()) {
      return;
    }
    batch.call();
    objectCollection.clear();
  }

  public static final class Builder<T> {

    private int batchSize = 50;
    private Function<T, Row> objectMapper;
    private Table table;

    private Builder(Table table) {
      this.table = table;
    }

    /**
     * Set batch size.
     */
    public Builder<T> withBatchSize(final int batchSize) {
      this.batchSize = batchSize;
      return this;
    }

    /**
     * Set object collection element mapper. This function should map item from collection
     * into instance of {@link Row}.
     */
    public Builder<T> withMapper(final Function<T, Row> objectMapper) {
      this.objectMapper = objectMapper;
      return this;
    }

    /**
     * Build batch instance.
     */
    public OnDemandBatch<T> build() {
      requireNonNull(table, "HBase Table not set");
      requireNonNull(objectMapper, "Object to HBase row instance mapper not set");
      return new OnDemandBatch<>(this);
    }
  }
}
