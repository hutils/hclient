package org.hclient.table;

import org.apache.hadoop.hbase.util.Bytes;

/**
 * Class represent HBase column identifier, defined by column family and qualifier.
 */
public final class Column {

  private final byte[] family;
  private final byte[] qualifier;

  private Column(byte[] family, byte[] qualifier) {
    this.family = family;
    this.qualifier = qualifier;
  }

  /**
   * Create new column identifier.
   *
   * @param family    Column family
   * @param qualifier Column qualifier
   * @return Column identifier instance.
   */
  public static Column of(byte[] family, byte[] qualifier) {
    return new Column(family, qualifier);
  }

  /**
   * Create new column identifier.
   *
   * @param family    Column family
   * @param qualifier Column qualifier
   * @return Column identifier instance.
   */
  public static Column of(String family, byte qualifier) {
    return new Column(Bytes.toBytes(family), Bytes.toBytes(qualifier));
  }

  /**
   * Create new column identifier.
   *
   * @param family    Column family
   * @param qualifier Column qualifier
   * @return Column identifier instance.
   */
  public static Column of(String family, short qualifier) {
    return new Column(Bytes.toBytes(family), Bytes.toBytes(qualifier));
  }

  /**
   * Create new column identifier.
   *
   * @param family    Column family
   * @param qualifier Column qualifier
   * @return Column identifier instance.
   */
  public static Column of(String family, String qualifier) {
    return new Column(Bytes.toBytes(family), Bytes.toBytes(qualifier));
  }

  /**
   * Create new column identifier.
   *
   * @param family    Column family
   * @param qualifier Column qualifier
   * @return Column identifier instance.
   */
  public static Column of(String family, int qualifier) {
    return new Column(Bytes.toBytes(family), Bytes.toBytes(qualifier));
  }

  /**
   * Create new column identifier.
   *
   * @param family    Column family
   * @param qualifier Column qualifier
   * @return Column identifier instance.
   */
  public static Column of(String family, long qualifier) {
    return new Column(Bytes.toBytes(family), Bytes.toBytes(qualifier));
  }

  /**
   * Column family.
   */
  public byte[] family() {
    return family;
  }

  /**
   * Column qualifier.
   */
  public byte[] qualifier() {
    return qualifier;
  }
}
