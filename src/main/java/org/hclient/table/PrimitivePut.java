package org.hclient.table;

import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Extended Put class which support operations on primitive types.
 */
public final class PrimitivePut extends Put {

  /**
   * Create instance with passed row key.
   * @param row Row key.
   */
  public PrimitivePut(byte row) {
    this(Bytes.toBytes(row));
  }

  /**
   * Create instance with passed row key.
   * @param row Row key.
   */
  public PrimitivePut(short row) {
    this(Bytes.toBytes(row));
  }

  /**
   * Create instance with passed row key.
   * @param row Row key.
   */
  public PrimitivePut(int row) {
    this(Bytes.toBytes(row));
  }

  /**
   * Create instance with passed row key.
   * @param row Row key.
   */
  public PrimitivePut(long row) {
    this(Bytes.toBytes(row));
  }

  /**
   * Create instance with passed row key.
   * @param row Row key.
   */
  public PrimitivePut(String row) {
    this(Bytes.toBytes(row));
  }

  /**
   * Create instance with passed row key.
   * @param row Row key.
   */
  public PrimitivePut(byte[] row) {
    super(row);
  }

  /**
   * Create instance based on existing {@link Put} instance.
   *
   * @param put {@link Put} instance.
   */
  public PrimitivePut(Put put) {
    super(put);
  }

  // --- byte ---

  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, byte value) {
    super.addColumn(family, qualifier, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, long ts, byte value) {
    super.addColumn(family, qualifier, ts, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, byte value) {
    super.addColumn(column.family(), column.qualifier(), Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, long ts, byte value) {
    super.addColumn(column.family(), column.qualifier(), ts, Bytes.toBytes(value));
    return this;
  }

  // --- short ---
  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, short value) {
    super.addColumn(family, qualifier, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, long ts, short value) {
    super.addColumn(family, qualifier, ts, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, short value) {
    super.addColumn(column.family(), column.qualifier(), Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, long ts, short value) {
    super.addColumn(column.family(), column.qualifier(), ts, Bytes.toBytes(value));
    return this;
  }

  // --- int ---
  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, int value) {
    super.addColumn(family, qualifier, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, long ts, int value) {
    super.addColumn(family, qualifier, ts, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, int value) {
    super.addColumn(column.family(), column.qualifier(), Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, long ts, int value) {
    super.addColumn(column.family(), column.qualifier(), ts, Bytes.toBytes(value));
    return this;
  }

  // --- long ---
  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, long value) {
    super.addColumn(family, qualifier, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, long ts, long value) {
    super.addColumn(family, qualifier, ts, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, long value) {
    super.addColumn(column.family(), column.qualifier(), Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, long ts, long value) {
    super.addColumn(column.family(), column.qualifier(), ts, Bytes.toBytes(value));
    return this;
  }

  // --- string ---
  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, String value) {
    super.addColumn(family, qualifier, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param family Column family
   * @param qualifier Column qualifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(byte[] family, byte[] qualifier, long ts, String value) {
    super.addColumn(family, qualifier, ts, Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, String value) {
    super.addColumn(column.family(), column.qualifier(), Bytes.toBytes(value));
    return this;
  }

  /**
   * Add new column to {@link Put} instance.
   *
   * @param column Column identifier
   * @param ts Column timestamp
   * @param value Column value
   *
   * @return This instance
   */
  public PrimitivePut addColumn(Column column, long ts, String value) {
    super.addColumn(column.family(), column.qualifier(), ts, Bytes.toBytes(value));
    return this;
  }

  // --- other significant Put methods ---

  /**
   * Set the desired durability for this mutation.
   * @param durability Required durability value
   */
  public PrimitivePut setDurability(Durability durability) {
    super.setDurability(durability);
    return this;
  }

  /**
   * Set the TTL desired for the result of the mutation, in milliseconds.
   *
   * @param ttl TTL value
   */
  public PrimitivePut setTtl(long ttl) {
    super.setTTL(ttl);
    return this;
  }
}
