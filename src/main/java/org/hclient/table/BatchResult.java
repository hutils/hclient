package org.hclient.table;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;

/**
 * Class container for HBase batch API operation.
 */
public final class BatchResult {

  private final Object[] batchResults;

  public BatchResult(int batchSize) {
    this.batchResults = new Object[batchSize];
  }

  private static String getStackTraceAsString(Throwable throwable) {
    final var errors = new StringWriter();
    throwable.printStackTrace(new PrintWriter(errors));
    return errors.toString();
  }

  public Object[] asArray() {
    return batchResults;
  }

  /**
   * Method throws exception if batch operation failed.
   *
   * @throws IOException Exception thrown if some batch part is failed.
   */
  public void throwIfFailed() throws IOException {
    for (int i = 0; i < batchResults.length; i++) {
      if (batchResults[i] instanceof Throwable) {
        throw new IOException("One of batch operations raise exception",
                              (Throwable) batchResults[i]);
      }
      if (batchResults[i] == null) {
        throw new IOException(MessageFormat.format("Communication error happened "
                                                   + "on some batch operation(op index={0})", i));
      }
    }
  }

  /**
   * Method indicate is passed batch failed.
   */
  public boolean isFailed() {
    for (int i = 0; i < batchResults.length; i++) {
      if (batchResults[i] instanceof Throwable || batchResults[i] == null) {
        return true;
      }
    }
    return false;
  }

  /**
   * Return string representation of all batch errors.
   */
  public String getErrors() {
    final StringBuilder errorBuilder = new StringBuilder();
    for (int i = 0; i < batchResults.length; i++) {
      if (batchResults[i] instanceof Throwable) {
        errorBuilder.append("One of batch operations raise exception:");
        errorBuilder.append(System.lineSeparator());
        errorBuilder.append(getStackTraceAsString((Throwable) batchResults[i]));
        errorBuilder.append(System.lineSeparator());
        errorBuilder.append(System.lineSeparator());
      }
      if (batchResults[i] == null) {
        errorBuilder.append("Communication error happened " + "on some batch operation(op index=");
        errorBuilder.append(i);
        errorBuilder.append(')');
        errorBuilder.append(System.lineSeparator());
        errorBuilder.append(System.lineSeparator());
      }
    }
    return errorBuilder.toString();
  }
}
